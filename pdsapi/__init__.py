import socket
import time


# PDS is open source software based on pcsd publiched license GNU GPL3

class pds:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def set(self, key, value):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('set'.encode())
        time.sleep(0.1)
        sock.send(key.encode())
        time.sleep(0.1)
        sock.send(value.encode())

    def get(self, key):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('get'.encode())
        time.sleep(0.1)
        sock.send(key.encode())
        result = sock.recv(2048)
        result = result.decode()
        return result

    def exit(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('exit'.encode())

    def clear(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('clear'.encode())

    def ping(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('ping'.encode())
        result = sock.recv(2048)
        result = result.decode()
        return result

    def rm(self, key):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('rm'.encode())
        key = key.encode()
        time.sleep(0.1)
        sock.send(key)

    def plus(self, key, value_plus):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('plus'.encode())
        time.sleep(0.1)
        sock.send(key.encode())
        time.sleep(0.1)
        sock.send(value_plus.encode())

    def rename(self, key, keynew):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.ip, self.port))
        sock.send('rename'.encode())
        time.sleep(0.1)
        sock.send(key.encode())
        time.sleep(0.1)
        sock.send(keynew.encode())
