from setuptools import setup

setup(
    name='pds-api',
    version='1.1',
    description='Pds api',
    url='https://git.glebmail.xyz/PythonPrograms/pds',
    author='gleb',
    packages=['pdsapi'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
)
